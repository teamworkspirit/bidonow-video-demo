const { saveVideoToS3 } = require('./saveVideoToS3');

module.exports.videoControllers = {
  saveVideoToS3,
};
