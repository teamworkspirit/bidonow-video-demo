const { v4: uuid } = require('uuid');
const { S3 } = require('../../tools/S3');

const { ServerError } = require('../../utils/error');

const ALLOWED_FORMATS = [
  'mp4',
  'mov',
  'webm'
]

module.exports.saveVideoToS3 = async (req, res) => {
  try {
    const { files } = req;
    const { video } = files;

    const videoFormat = (video.type || video.mimetype).split('/')[1];

    if (!ALLOWED_FORMATS.includes(videoFormat)) {
      throw new ServerError(`Please upload ${ALLOWED_FORMATS.map(f => `.${f}`).join('/')} video file only`, 403)
    }

    if (video.size < process.env.VIDEO_MIN_SIZE * 1e6) {
      throw new ServerError(`Please upload ${process.env.VIDEO_MIN_SIZE}mb files size minimum requirements`, 403)
    }

    if (video.size > process.env.VIDEO_MAX_SIZE * 1e6) {
      throw new ServerError(`Please upload ${process.env.VIDEO_MIN_SIZE}mb files size maximum requirements`, 403)
    }

    const S3Res = await S3
      .upload({
        Bucket: process.env.S3_VIDEO_BUCKET,
        Key: video.name === 'blob' ? `VIDEO-${uuid()}.${videoFormat}` : video.name,
        Body: video.data
      })
      .promise()

    res.json(S3Res);
  } catch (err) {
    console.log("err", err)
    res.status(err.statusCode).json({ error: err.statusCode === 500 ? 'Internal Error' : err.message })
  }
};