require('dotenv').config();

const express = require('express');
const bodyParser = require('body-parser')
const path = require('path');
const logger = require('morgan');
const cors = require('cors')
const fileUpload = require('express-fileupload');

const app = express();

app.use(fileUpload());

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(cors())

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/', require('./routes'))

app.listen(process.env.APP_PORT, () => {
  console.log(`✔ App started on port ${process.env.APP_PORT}!`)
});
