const AWS_S3 = require('aws-sdk');

AWS_S3.config.update({
  endpoint: process.env.S3_ENDPOINT,
  region: process.env.AWS_REGION,
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
});

module.exports.S3 = new AWS_S3.S3({ s3BucketEndpoint: false, s3ForcePathStyle: true });