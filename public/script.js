// constants
const MAX_TICKS = 100;
const FORM_DATA_VIDEO_KEY = "video";
const SAVE_VIDEO_URL = "/api/video";
const titles = ["Tell us your story!", "Great Job"];

// DOM elements
const $video = document.getElementById("videoElement");
const $startButton = document.getElementById("start-record-btn");
const $fileInput = document.getElementById("file-input");
// const $counter = document.getElementById("count");
const $saveButton = document.getElementById("save-button");
const $cancelButton = document.getElementById("cancel-button");
const $removeButton = document.getElementById("remove-button");
const $progressBar = document.getElementById("progressBar");
const $success = document.getElementById("success");
const $title = document.getElementById("title");

const controls = [$startButton, $fileInput, $saveButton, $cancelButton, $removeButton];

// global variables
let videoToSave = null;
let recorder = null;
let progressBarIntervalID = null;

const addSubscriptions = () => {
	// start record button click event
	$startButton.addEventListener("click", startRecord);

	// handle file upload via input
	$fileInput.addEventListener("change", chooseLocalFile);

	// handle save
	$saveButton.addEventListener("click", handleSaveClick);

	// handle cancel
	$cancelButton.addEventListener("click", resetApp);

	// handle remove
	$removeButton.addEventListener("click", resetApp);
};

const startRecord = async () => {
	resetApp();
	$startButton.setAttribute("disabled", true);
	// $video.setAttribute("muted", true);

	const stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });

	recorder = new RecordRTCPromisesHandler(stream);
	// $video.srcObject = stream;

	const videoStream = stream.getVideoTracks()[0];
	const mediaStream = new MediaStream([videoStream]);

	$video.srcObject = mediaStream;
	$video.play();

	$video.classList.add("start-record");

	recorder.startRecording();
	countUp(stopRecord);

	showControlButtons();
	setText(titles[0]);
};

const stopRecord = async () => {
	recorder.stopRecording();
	const base64 = await recorder.getDataURL();
	videoToSave = await recorder.getBlob();

	const source = document.createElement("source");
	$video.srcObject = null;
	$video.appendChild(source);
	$video.removeAttribute("muted");
	source.setAttribute("src", base64);

	// const blob = await recorder.getBlob();
	// invokeSaveAsDialog(blob);
	recorder = null;
	$startButton.removeAttribute("disabled");
	// $counter.innerHTML = "";
	hideProgressBar();
};

const countUp = cb => {
	let width = 0;
	const tick = () => {
		if (width >= MAX_TICKS) {
			clearInterval(progressBarIntervalID);
			cb();
		} else {
			width++;
			$progressBar.style.width = width + "%";
			$progressBar.getElementsByTagName("span")[0].innerHTML = width * 1 + "%";
		}
	};
	progressBarIntervalID = setInterval(tick, 600);
};

const chooseLocalFile = e => {
	$video.setAttribute("muted", true);
	clearVideoArea();
	videoToSave = e.target.files[0];
	const source = document.createElement("source");
	source.setAttribute("src", URL.createObjectURL(videoToSave));
	$video.appendChild(source);

	$video.classList.add("start-record");
	showControlButtons();
	setText(titles[0]);
};

const handleSaveClick = async () => {
	hideProgressBar();
	if (!videoToSave && !recorder) {
		alert("Upload or record video first!");
		return;
	}

	if (recorder) await stopRecord();

	const formData = new FormData();
	formData.append(FORM_DATA_VIDEO_KEY, videoToSave);
	$success.classList.add("show");

	disableControls();

	try {
		await fetch(SAVE_VIDEO_URL, {
			method: "POST",
			body: formData
		});
		animateSuccess($saveButton);
		resetApp();
	} catch (err) {
		alert(err.statusCode);
	} finally {
		enableControls();
		setText(titles[1]);
	}
};

const setText = title => {
	$title.innerText = title;
};

const showControlButtons = () => {
	$saveButton.classList.add("show");
	$cancelButton.classList.add("show");
	$removeButton.classList.add("show");
};

const hideControlButtons = () => {
	$saveButton.classList.remove("show");
	$cancelButton.classList.remove("show");
	$removeButton.classList.remove("show");
};

const disableControls = () => controls.forEach(control => control.setAttribute("disabled", true));
const enableControls = () => controls.forEach(control => control.removeAttribute("disabled"));
const animateSuccess = ($element, color = "green") => {
	$element.style.background = color;
	setTimeout(() => {
		$element.style.background = "";
	}, 2000);
};
const clearVideoArea = () => {
	$video.srcObject = null;
	if ($video.childNodes.length !== 0) [...$video.childNodes].forEach(el => el.remove());
};
const clearFileInput = () => {
	$fileInput.value = "";
};
const hideProgressBar = () => {
	$progressBar.style.width = 0;
	$progressBar.getElementsByTagName("span")[0].innerHTML = "";
	clearInterval(progressBarIntervalID);
};
const resetApp = () => {
	$success.classList.remove("show");
	videoToSave = null;
	recorder = null;
	clearVideoArea();
	clearFileInput();
	hideProgressBar();
	hideControlButtons();
	setText(titles[0]);
	enableControls();
};

(() => {
	// main logic is placed here
	addSubscriptions();
})();
