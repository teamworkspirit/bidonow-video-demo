const express = require('express');
const router = express.Router(); 

// dependent routes
const videoRoutes = require('./video');

router.use('/video', videoRoutes)

module.exports = router;
