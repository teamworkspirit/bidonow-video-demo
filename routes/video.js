const express = require('express');

// related controllers
const { videoControllers } = require('../controllers/video');

const router = express.Router();

// routes
router.post('/', videoControllers.saveVideoToS3);

module.exports = router;
